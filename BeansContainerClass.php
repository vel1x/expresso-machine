<?php

namespace SoConnect\Coffee;


class BeansContainerClass implements BeansContainer
{
    use ContainerTrait;

    public function addBeans(int $numSpoons): void
    {
        $this->addAmount($numSpoons);
    }

    public function useBeans(int $numSpoons): int
    {
        if($this->getAmount() - $numSpoons < 0) {
            throw new NoBeansException();
        }

        $this->removeAmount($numSpoons);

        return $numSpoons;
    }

    public function getBeans(): int
    {
        return $this->getAmount();
    }
}
