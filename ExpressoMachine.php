<?php

namespace SoConnect\Coffee;


class ExpressoMachine implements EspressoMachineInterface
{
    const WATER_EXPRESSO = 0.05;

    const WATER_EXPRESSO_DOUBLE = 0.10;

    const SPOON_BEANS_EXPRESSO = 1;

    const SPOON_BEANS_EXPRESSO_DOUBLE = 2;

    private $beansContainer;

    private $waterContainer;

    public function __construct(BeansContainer $beansContainer, WaterContainer $waterContainer)
    {
        $this->beansContainer = $beansContainer;
        $this->waterContainer = $waterContainer;
    }

    public function makeEspresso() : float
    {
        return $this->makeCoffee(self::WATER_EXPRESSO, self::SPOON_BEANS_EXPRESSO);
    }

    public function makeDoubleEspresso() : float
    {
        return $this->makeCoffee(self::WATER_EXPRESSO_DOUBLE, self::SPOON_BEANS_EXPRESSO_DOUBLE);
    }

    private function makeCoffee(float $water, int $beans): float
    {
        $this->beansContainer->useBeans($beans);
        return $this->waterContainer->useWater($water);
    }

    public function getStatus() : string
    {
        switch (true) {
            case ($this->getWaterContainer()->getWater() < self::WATER_EXPRESSO
                && $this->getBeansContainer()->getBeans() < self::SPOON_BEANS_EXPRESSO):
                $message = 'Add beans and water';
                break;
            case ($this->getBeansContainer()->getBeans() < self::SPOON_BEANS_EXPRESSO):
                $message = 'Add beans';
                break;
            case (!$this->getWaterContainer()->isMainWaterSupply()
                && $this->getWaterContainer()->getWater() < self::WATER_EXPRESSO):
                $message = 'Add water';
                break;
            default:
                $message = sprintf('%d Expressos left', $this->getExpressosLeft());
        }

        return $message;
    }

    private function getExpressosLeft(): int
    {
        if ($this->getWaterContainer()->isMainWaterSupply()) {
             return intval($this->getBeansContainer()->getBeans());
        }

        return min($this->getBeansContainer()->getBeans(), floor($this->getWaterContainer()->getWater() / self::WATER_EXPRESSO));
    }

    public function fillContainers(): void
    {
        $this->getBeansContainer()->fill();
        $this->getWaterContainer()->fill();
    }

    public function getWaterContainer(): WaterContainerClass
    {
        return $this->getWaterContainer();
    }

    public function getBeansContainer(): BeansContainerClass
    {
        return $this->getBeansContainer();
    }
}
