<?php

namespace SoConnect\Coffee;


trait ContainerTrait
{
    protected $amount;

    protected $capacity;

    public function __construct($capacity)
    {
        $this->capacity = $capacity;

        $this->amount = 0;
    }

    protected function addAmount($amount): void
    {
        if($this->amount + $amount > $this->capacity) {
            throw new ContainerFullException();
        }

        $this->amount += $amount;
    }

    protected function removeAmount($amount): void
    {
        $this->amount -= $amount;
    }

    protected function getAmount()
    {
        return $this->amount;
    }

    public function getCapacity()
    {
        return $this->capacity;
    }

    public function fill(): void
    {
        $this->amount = $this->capacity;
    }
}
