<?php

namespace SoConnect\Coffee;


class WaterContainerClass implements WaterContainer
{
    use ContainerTrait;

    private $mainWaterSupply = false;

    public function addWater(float $litres): void
    {
        $this->addAmount($litres);
    }

    public function useWater(float $litres): float
    {
        if(!$this->mainWaterSupply && ($this->getAmount() - $litres <= 0)) {
            throw new NoWaterException();
        }

        $this->removeAmount($litres);

        return $litres;
    }

    public function getWater(): float
    {
        return $this->getAmount();
    }

    public function setMainWaterSupply(): void
    {
        $this->mainWaterSupply = true;
    }

    public function isMainWaterSupply(): bool
    {
        return $this->mainWaterSupply;
    }
}
